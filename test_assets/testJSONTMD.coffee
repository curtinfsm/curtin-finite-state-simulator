data = '''
{
  "machineType": 3,
  "startState": "state1",
  "states": [
    {
    "name": "state1",
    "acceptState": null,
    "x": 225.90625,
    "y": 188.8125,
    "transitions": [
      {     
      "readTape": 0,
      "writeTape": 1,
      "readVal": "a",
      "writeVal": "",
      "moveRead": "R",
      "moveWrite": "R",
      "destination": "state3"
      },
      {
      "readTape": 0,
      "writeTape": 1,
      "readVal": "b",
      "writeVal": "x",
      "moveRead": "R",
      "moveWrite": "R",
      "destination": "state2"
      }
      ]
    },
    {
    "name": "state2",
    "acceptState": true,
    "x": 508.90625,
    "y": 198.8125,
    "transitions": [
      ]
    },
    {
    "name": "state3",
    "acceptState": true,
    "x": 508.90625,
    "y": 198.8125,
    "transitions": [
      ]
    }
  ]
}
'''

module.exports = data
