data = '''
{
    "machineType": 0,
    "startState": "state1",
    "states": [
        {
            "name": "state1",
      "x": 225.90625,
      "y": 188.8125,
            "transitions": [
                {
                    "condition": "0",
                    "destination": "state1"
                },
                {
                    "condition": "1",
                    "destination": "state2"
                }
            ]
        },
        {
            "name": "state2",
      "x": 508.90625,
      "y": 198.8125,
            "transitions": [
                {
                    "condition": "0",
                    "destination": "state2"
                },
                {
                    "condition": "1",
                    "destination": "state1"
                },
                {
                    "condition": "X",
                    "destination": "state3"
                }
            ]
        },
        {
            "name": "state3",
      "x": 368.90625,
      "y": 358.8125,
            "transitions": [
                {
                    "condition": "0",
                    "destination": "state1"
                }
            ]
        }
    ]
}
'''

module.exports = data
