d3data = {
    "tapes": [
      [1,2,3,4,null],
      [6,"h","k",9,null]
    ],
    "states": [
        {
            "name": "state0",
            "acceptState": null,
            "transitions": [
                {
                    "condition": {
                      "readTape": "0",
                      "writeTape": "1",
                      "readCond": "a",
                      "writeVal": "Ø",
                      "moveRead": "L",
                      "moveWrite": "R"
                    },
                    "destination": "state1"
                },
                {
                    "readTape": "0",
                    "writeTape": "1",
                    "condition": "0",
                    "write": "c",
                    "move": "R",
                    "destination": "state0",
                }
            ],
            "x": 125.40625,
            "y": 195.8125
        },
        {
            "name": "state1",
            "transitions": [
                {
                    "readTape": "0",
                    "writeTape": "1",
                    "condition": "0",
                    "write": "c",
                    "move": "S",
                    "destination": "state0",
                },
                {
                    "readTape": "0",
                    "writeTape": "1",
                    "condition": "1",
                    "write": "c",
                    "move": "R",
                    "destination": "state1",
                }
            ],
            "x": 499.40625,
            "y": 96.8125
        }
    ],
    "startState": "state0"
}


module.exports = d3data
